# Object-Oriented Programming (OOP) Concepts in JavaScript

  
## Introduction

JavaScript is a versatile programming language commonly used for web development. One of its key features is its support for Object-Oriented Programming (OOP) principles. In this paper, we will delve into the fundamentals of OOP in JavaScript, including objects, classes, inheritance, encapsulation, and polymorphism.

## Objects
In JavaScript, an object is a collection of properties, each of which has a name and a value. Objects can also contain methods, which are functions stored as object properties. Objects can be created using object literals or constructed using constructor functions.

```javascript
//Object literal
let  person  = {
	name: "John",
	age: 30,
	greet: function() {
		console.log("Hello, my name is "  +  this.name);
	}
};

// Constructor function
function  Person(name, age) {
	this.name  =  name;
	this.age  =  age;
	this.greet  =  function() {
	console.log(`Hello, my name is ${this.name}`);
	}
}
let  person1  =  new  Person("Alice", 25);
person1.greet();
```

## Classes
ES6  introduced  class  syntax  to  JavaScript, providing  a  more  familiar  syntax  for  defining  classes  and  creating  objects. Classes  encapsulate  data  for  the  object  and  define  methods  to  operate  on  that  data.
```js
class  Rectangle {
	constructor(width, height) {
		this.width  =  width;
		this.height  =  height;
	}
	calculateArea() {
		return  this.width  *  this.height;
	}
}
let  rect  =  new  Rectangle(5, 10);

console.log(rect.calculateArea());

```

  

## Inheritance

Inheritance allows objects to inherit properties and methods from another object. In JavaScript, inheritance is achieved through prototype chaining, where objects inherit properties and methods from their prototype.

```javascript
class  Animal {
	constructor(name) {
		this.name  =  name;
	}
	speak() {
		console.log(this.name  +  " makes a sound.");
	}
}

class  Dog  extends  Animal {
	constructor(name, breed) {
		super(name);
		this.breed  =  breed;
	}
	speak() {
		console.log(this.name  +  " barks.");
	}
}

let  dog  =  new  Dog("Buddy", "Labrador");
dog.speak();

```

## Encapsulation

Encapsulation is the bundling of data and methods that operate on that data within a single unit. In JavaScript, encapsulation is achieved through closures and private properties.
```javascript
function  Counter() {
	let  count  =  0;
	this.increment  =  function() {
	count++;
};
this.decrement  =  function() {
	count--;
};

this.getCount  =  function() {
	return  count;
	};
}
let  counter  =  new  Counter();

counter.increment();
counter.increment();
console.log(counter.getCount());

```

## Polymorphism

Polymorphism allows objects to be treated as instances of their parent class, enabling code to be written that operates on objects of various types.
```javascript
class  Shape {
	draw() {
		console.log("Drawing a shape.");
	}
}
class  Circle  extends  Shape {
	draw() {
		console.log("Drawing a circle.");
	}
}

class  Square  extends  Shape {
	draw() {
		console.log("Drawing a square.");
	}
}

function  drawShape(shape) {
	shape.draw();
}
let  circle  =  new  Circle();
let  square  =  new  Square();
drawShape(circle);
drawShape(square);
```

## Conclusion

Object-Oriented Programming in JavaScript provides a powerful paradigm for organizing and structuring code. Understanding these concepts allows developers to write more maintainable, scalable, and reusable code

## References

* [Classes in Javascript](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Classes_in_JavaScript)
* [OOPs in Javascript](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Object-oriented_programming)
* [More on OOPs](https://www.freecodecamp.org/news/object-oriented-javascript-for-beginners/)
